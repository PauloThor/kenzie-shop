import { addProduct } from "./actions";

const addProductThunk = (product) => {
  return (dispatch) => {
    dispatch(addProduct(product));
  };
};

export default addProductThunk;
