import { ADD_PRODUCT } from "./actionTypes";

const products = [
  { name: "Echo Dot", price: 279.0, image: "link-da-imagem", id: 1 },
  { name: "Kindle Paperwhite", price: 499.0, image: "link-da-imagem", id: 2 },
  { name: "Lorem Exlusive", price: 619.0, image: "link-da-imagem", id: 3 },
  { name: "Redux Package", price: 199.0, image: "link-da-imagem", id: 4 },
  { name: "Thunk Premium", price: 1199.0, image: "link-da-imagem", id: 5 },
];

const productsReducer = (state = products, action) => {
  switch (action.type) {
    case ADD_PRODUCT:
      return [...state, action.product];

    default:
      return state;
  }
};

export default productsReducer;
