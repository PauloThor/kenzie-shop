import { ADD_CART, REMOVE_CART, CLEAR_CART } from "./actionTypes";

const defaultState = JSON.parse(localStorage.getItem("cart")) || [];
const defaultCost = JSON.parse(localStorage.getItem("cost")) || 0;

const cartReducer = (
  state = { products: defaultState, cost: defaultCost },
  action
) => {
  const newCost = () => {
    return state.cost - action.product.price;
  };

  const newCart = () => {
    const array = state.products;

    // if (!array.includes(action.product)) {
    //   return array;
    // }

    const index = array.indexOf(action.product);
    const removed = array.splice(index, 1);
    console.log("removed: ", removed);
    console.log("array: ", array);
    return array;
  };

  switch (action.type) {
    case ADD_CART:
      return {
        products: [...state.products, action.product],
        cost: state.cost + action.product.price,
      };

    case REMOVE_CART:
      return {
        products: newCart(),
        cost: newCost(),
      };

    case CLEAR_CART:
      return {
        products: [],
        cost: 0,
      };

    default:
      return state;
  }
};

export default cartReducer;
