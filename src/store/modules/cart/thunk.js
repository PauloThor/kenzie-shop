import { addCart, removeCart } from "./actions";

export const addCartThunk = (product) => {
  return (dispatch) => {
    const list = JSON.parse(localStorage.getItem("cart")) || [];
    let cost = JSON.parse(localStorage.getItem("cost")) || 0;

    list.push(product);
    localStorage.setItem("cart", JSON.stringify(list));

    cost += product.price;
    localStorage.setItem("cost", JSON.stringify(cost));

    dispatch(addCart(product));
  };
};

export const removeCartThunk = (product) => {
  return (dispatch) => {
    const list = JSON.parse(localStorage.getItem("cart")) || [];
    let cost = JSON.parse(localStorage.getItem("cost")) || 0;

    const index = list.indexOf(product);
    const removed = list.splice(index, 1);
    console.log("removed:", removed);

    localStorage.setItem("cart", JSON.stringify(list));

    cost -= product.price;
    localStorage.setItem("cost", JSON.stringify(cost));
    console.log("list: ", list);
    dispatch(removeCart(product));
  };
};
