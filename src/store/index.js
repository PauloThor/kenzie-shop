import { createStore, combineReducers, applyMiddleware } from "redux";

import thunk from "redux-thunk";
import cartReducer from "./modules/cart/reducer";
import productsReducer from "./modules/Products/reducer";

// import storeSynchronize from "redux-localstore";

const reducers = combineReducers({
  products: productsReducer,
  cart: cartReducer,
});

const store = createStore(reducers, applyMiddleware(thunk));

// storeSynchronize(store);

export default store;
