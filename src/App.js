import "./App.css";
import Cart from "./components/Cart";
import Products from "./components/Products";

function App() {
  return (
    <div className="App" style={{ display: "flex", justifyContent: "center" }}>
      <div style={{ marginRight: "20px" }}>
        {" "}
        <Products />
      </div>
      <Cart />
    </div>
  );
}

export default App;
