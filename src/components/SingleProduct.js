// import { useState } from "react";
import { Button, makeStyles } from "@material-ui/core";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addCartThunk, removeCartThunk } from "../store/modules/cart/thunk";

const useStyles = makeStyles(() => ({
  list: {
    listStyle: "none",
    border: "2px solid #303F9F",
    margin: "10px",
    padding: "10px",
    borderRadius: "10px",
  },
  name: {
    background: "gray",
    padding: "5px",
    borderRadius: "10px",
    color: "white",
  },
  price: {
    border: "1px solid #303F9F",
    padding: "5px",
    borderRadius: "10px",
  },
}));

const SingleProduct = ({ product }) => {
  // const [cartStorage] = useState(
  //   JSON.parse(localStorage.getItem("cart")) || []
  // );
  const [exists, setExists] = useState(true);
  const classes = useStyles();

  const dispatch = useDispatch();

  const { cart } = useSelector((state) => state);

  useEffect(() => {
    setExists(!cart.products.includes(product));
  }, [cart, product]);

  const handleRemove = (product) => {
    // if (!cart.products.includes(product)) {
    //   return;
    // }

    dispatch(removeCartThunk(product));
  };

  return (
    <li className={classes.list}>
      <p className={classes.name}>{product.name}</p>
      <p className={classes.price}>Preço: R${product.price}</p>
      <div style={{ margin: "10px" }}>
        <Button
          onClick={() => dispatch(addCartThunk(product))}
          color="primary"
          variant="contained"
        >
          Adicionar ao carrinho
        </Button>
      </div>
      <div>
        <Button
          onClick={() => handleRemove(product)}
          disabled={exists}
          color="secondary"
          variant="outlined"
        >
          Remover do carrinho
        </Button>
      </div>
    </li>
  );
};

export default SingleProduct;
