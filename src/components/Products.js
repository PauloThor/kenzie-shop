import { useSelector } from "react-redux";
import SingleProduct from "./SingleProduct";

const Products = () => {
  const { products } = useSelector((state) => state);

  return (
    <div>
      <h2>Lista de Produtos</h2>
      <ul style={{ display: "flex", flexWrap: "wrap" }}>
        {products.map((product, i) => (
          <SingleProduct product={product} key={i} />
        ))}
      </ul>
    </div>
  );
};

export default Products;
