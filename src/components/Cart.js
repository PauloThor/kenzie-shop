import { useDispatch, useSelector } from "react-redux";
import { clearCart } from "../store/modules/cart/actions";

import DeleteIcon from "@material-ui/icons/Delete";
import { Button, makeStyles, Modal } from "@material-ui/core";
import { useState } from "react";

const useStyles = makeStyles(() => ({
  buttonContainer: {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%)",
    display: "flex",
    background: "white",
    padding: "20px",
    borderRadius: "15px",
  },
  button: {
    marginRight: "10px",
  },
  item: {
    padding: "10px 0",
    margin: "10px",
    backgroundColor: "gray",
    color: "white",
    borderRadius: "10px",
  },
}));

const Cart = () => {
  const { cart } = useSelector((state) => state);
  const [isShowing, setIsShowing] = useState(false);
  const dispatch = useDispatch();

  const classes = useStyles();

  const handleClear = () => {
    localStorage.removeItem("cart");
    localStorage.removeItem("cost");
    dispatch(clearCart());
    handleShow();
  };

  const handleShow = () => {
    setIsShowing(!isShowing);
  };

  return (
    <div style={{ paddingRight: "20px" }}>
      <h2>Carrinho</h2>
      <Modal open={isShowing} onClose={() => setIsShowing(false)}>
        <div className={classes.buttonContainer}>
          <Button
            color="primary"
            variant="contained"
            className={classes.button}
            onClick={handleShow}
          >
            Mudei de ideia
          </Button>
          <Button color="secondary" variant="outlined" onClick={handleClear}>
            Sim, quero esvaziar
          </Button>
        </div>
      </Modal>
      <Button
        variant="contained"
        color="secondary"
        startIcon={<DeleteIcon />}
        onClick={handleShow}
      >
        Esvaziar carrinho
      </Button>
      <ul style={{ listStyle: "none", paddingLeft: 0 }}>
        {cart.products &&
          cart.products.map((product, i) => (
            <li className={classes.item} key={i}>
              {product.name}
            </li>
          ))}
      </ul>
      <p>Custo Total: R${cart.cost}</p>
    </div>
  );
};

export default Cart;
